
> **_Activité_**

cette activité débranchée nos permet de s'initier aux algorithmes. 
on commence par découvrir les règles du jeu, puis la question de chercher à gagner se pose, et on découvre alors une solution répétitive qui fonctionne mécaniquement.

**règle du jeu :** deux joueurs ramassent tour à tour 1, 2 ou 3 allumettes sur une table. Celui qui prend
la dernière à gagné. A travers ce jeu bien connu, nous introduisons la notion d’algorithme comme stratégie gagnante pour résoudre un problème
