**Thématique :** Algorithmique

**Résumé de l’activité :** on commence par découvrir les règles du jeu, puis la question de chercher à gagner se pose, et on découvre alors une solution répétitive qui fonctionne mécaniquement.

**Objectifs :** introduire la notion d’algorithme comme stratégie gagnante pour résoudre un problème

**Auteur :** Sara sbaila

**Durée de l’activité :** 30 min 

**Forme de participation :** en binôme.

**Matériel nécessaire :** Des graines OU des billes OU des jetons OU des allumettes ou tout autre objet

**Préparation :** Aucune

**Autres références :** https://www.youtube.com/watch?v=3WIghG_B4nU&feature=emb_logo
