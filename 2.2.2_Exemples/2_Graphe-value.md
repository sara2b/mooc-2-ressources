# Une activité hybride sur la recherche de chemin dans un graphe

Thierry Viéville propose cette activité hybride sur la modélisation et la recherche d'un chemin optimal dans un graphe valué.

La fiche élève est constituée d'une première suite d'activités en ligne sur la modélisation d'un graphe. Fiche proposée par D. Roche et que l'on peut trouver ici : https://pixees.fr/informatiquelycee/n_site/nsi_term_structDo_graphe.html

La deuxième partie est l'activité hybride dont il est question. La fiche analyse est donc aussi la fiche élève : [accéder à la fiche](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/fiche-analyse-nsi-term-graphe-algo.md)

Nous pouvons échanger sur cet exemple dans [cette discussion dédiée du forum](https://mooc-forums.inria.fr/moocnsi/t/introduction-a-lalgorithme-de-dijkstra/3467).
